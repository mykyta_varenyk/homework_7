package ua.dnipro.ntudp.varenyk_mykyta.library.model;


import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Role entity.
 *
 * @author Mykyta Varenyk
 *
 */

public enum Role {
    ADMIN,
    USER,
    LIBRARIAN;

    public static SimpleGrantedAuthority getAuthority(User user){
        Role role = getRole(user);
        return  new SimpleGrantedAuthority("ROLE_" + role.getName());
    }

    public static Role getRole(User user){
        return Role.valueOf(user.getRole().toUpperCase());
    }

    public String getName() {
        return name();
    }
}
