package ua.dnipro.ntudp.varenyk_mykyta.library.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.exceptions.UnauthorizedException;


@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LogManager.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(Exception exception){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.INTERNAL_SERVER_ERROR.value());

        modelAndView.addObject("errorMessage","unexpected internal server error");

        LOGGER.debug("exception of type",exception);

        return modelAndView;
    }

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(RuntimeException exception){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.INTERNAL_SERVER_ERROR.value());

        modelAndView.addObject("errorMessage","unexpected internal server error");

        LOGGER.debug("exception of type",exception);

        return modelAndView;
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ModelAndView handleError(UnauthorizedException e){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.UNAUTHORIZED.value());

        modelAndView.addObject("errorMessage","you have no permission to access this resource!");

        LOGGER.debug(e);

        return modelAndView;
    }
}
