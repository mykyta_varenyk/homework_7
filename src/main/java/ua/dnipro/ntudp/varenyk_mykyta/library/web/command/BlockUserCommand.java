package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Block user.
 *
 * @author Mykyta Varenyk
 *
 */

@Service
public class BlockUserCommand extends Command{
    private UserDao userDao;
    private static Logger LOG = LogManager.getLogger(BlockUserCommand.class);

    @Autowired
    public BlockUserCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOG.debug("BlockUserCommand started");
        int userId = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        LOG.debug("user id -> {}",userId);

        try {
            userDao = new UserDao(DBManager.getDataSource());
        } catch (NamingException e) {
            e.printStackTrace();
        }

        boolean result = userDao.blockUser(userId);

        LOG.debug("user has been blocked -> {}",result);

        LOG.debug("BlockUserCommand finished");

        return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
    }
}
