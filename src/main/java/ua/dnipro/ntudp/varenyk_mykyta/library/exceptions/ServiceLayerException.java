package ua.dnipro.ntudp.varenyk_mykyta.library.exceptions;

public class ServiceLayerException extends RuntimeException{
    public ServiceLayerException(String message){
        super(message);
    }

    public ServiceLayerException(){
        super();
    }
}
