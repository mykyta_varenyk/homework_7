package ua.dnipro.ntudp.varenyk_mykyta.library.bpp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

@Component
public class TimedAnnotationBeanPostProcessor implements BeanPostProcessor {
    private Map<String,Class> map = new HashMap<>();

    private static final Logger LOGGER = LogManager.getLogger(TimedAnnotationBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();

        if (beanClass.isAnnotationPresent(Timed.class)) {
            map.put(beanName,beanClass);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = map.get(beanName);

        if (beanClass != null){
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    long before = System.currentTimeMillis();

                    Object retVal = method.invoke(bean,args);

                    long after = System.currentTimeMillis();

                    LOGGER.debug("{} executed in {} ",method.getName(),(after-before));

                    return retVal;
                }
            });
        }
        return bean;
    }
}

