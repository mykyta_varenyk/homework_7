package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Register command.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class RegisterCommand extends Command {
    private static final Logger LOGGER = LogManager.getLogger(RegisterCommand.class);

    private UserDao userDao;

    @Autowired
    public RegisterCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
           LOGGER.debug("RegisterCommand started");

           HttpSession session = req.getSession();

           String languageCode,errorMessage;

           if (session != null){
             languageCode = (String) session.getAttribute("language");
           }else {
            languageCode = "en";
           }

           LOGGER.debug("language code -> {}",languageCode);

        UserDao userDao = null;
        try {
            userDao = new UserDao(DBManager.getDataSource());
        } catch (NamingException e) {
            e.printStackTrace();
        }

        if (userDao.loginIsPresent(req.getParameter(Fields.USER_LOGIN))){
             errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                     .getString("label.loginIsPresentError");

             session.setAttribute("loginIsPresentError",errorMessage);

             LOGGER.error("errorMessage -> {}",errorMessage);

             return Path.PAGE_REGISTER;
          }

         if (!Util.validatePassword(req.getParameter(Fields.USER_PASSWORD))){
            errorMessage = ResourceBundle.getBundle("text",Locale.forLanguageTag(languageCode))
                    .getString("label.passwordPatternError");

            session.setAttribute("passwordPatternError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_REGISTER;
         }

         if (!"".equals(req.getParameter(Fields.USER_EMAIL)) && !Util.validateEmail(req.getParameter(Fields.USER_EMAIL))){
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.emailPatternError");

            session.setAttribute("emailPatternError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_REGISTER;
         }

        if (!"".equals(req.getParameter(Fields.USER_EMAIL)) && userDao.emailIsPresent(req.getParameter(Fields.USER_EMAIL))){
            errorMessage = ResourceBundle.getBundle("text",Locale.forLanguageTag(languageCode))
                    .getString("label.emailIsPresentError");

            session.setAttribute("emailIsPresentError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_REGISTER;
         }

           User user = getUser(req);

           boolean result = userDao.createUser(user);

           LOGGER.debug("user inserted -> {}",result);

           if (result == false )

           LOGGER.debug("RegisterCommand finished");

           return "/success.jsp";
    }

    private User getUser(HttpServletRequest req){
        User user = new User();

        user.setLogin(req.getParameter(Fields.USER_LOGIN));

        user.setPassword(req.getParameter(Fields.USER_PASSWORD));

        user.setEmail(req.getParameter(Fields.USER_EMAIL));

        user.setFirstName(req.getParameter(Fields.USER_FIRST_NAME));

        user.setLastName(req.getParameter(Fields.USER_LAST_NAME));

        user.setRoleId(2);

        LOGGER.debug("new user -> {}",user);

        return user;
    }
}
