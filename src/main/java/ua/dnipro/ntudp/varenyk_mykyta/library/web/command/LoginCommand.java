package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Login command.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class LoginCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("LoginCommand started");

        HttpSession session = req.getSession();

        String login = req.getParameter(Fields.USER_LOGIN);

        LOGGER.debug("login -> {}",login);

        String password = req.getParameter(Fields.USER_PASSWORD);

        String errorMessage, languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        String forward = Path.PAGE_ERROR_PAGE;

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password cannot be empty";
            LOGGER.error("errorMessage -> {}",errorMessage);
            req.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        User user = null;
        try {
            user = new UserDao(DBManager.getDataSource()).findUserByLogin(login);
        } catch (NamingException e) {
            e.printStackTrace();
        }

        if (user.getLogin() == null) {
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                            .getString("label.userNotFound");
            LOGGER.error("errorMessage -> {}",errorMessage);

            session.setAttribute("userNotFound",errorMessage);

            return Path.PAGE_LOGIN;
        } else if (!user.getPassword().equals(password)){
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.incorrectPasswordError");

            LOGGER.error("errorMessage -> {}",errorMessage);

            session.setAttribute("incorrectPasswordError",errorMessage);

            return Path.PAGE_LOGIN;
       }else if(user.isBlocked()) {
            errorMessage = "You are blocked!";
            LOGGER.error("errorMessage -> {}",errorMessage);
            req.setAttribute("errorMessage",errorMessage);
            return forward;
        } else {
            Role userRole = Role.getRole(user);

            if (userRole == Role.ADMIN){
                forward = Path.PAGE_ADMIN_HOME_PAGE_REDIRECT;
            }

            if (userRole == Role.USER){
                forward = Path.PAGE_USER_HOME_PAGE_REDIRECT;
            }

            if (userRole == Role.LIBRARIAN){
                forward = Path.PAGE_LIBRARIAN_HOME_PAGE_REDIRECT;
            }

            LOGGER.debug("userRole -> {}",userRole);

            session.setAttribute("user",user);

            session.setAttribute("userRole",userRole);

            LOGGER.debug("LoginCommand finished");

            return forward;
        }
    }
}