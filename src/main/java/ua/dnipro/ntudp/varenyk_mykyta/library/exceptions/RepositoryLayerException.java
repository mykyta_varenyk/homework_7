package ua.dnipro.ntudp.varenyk_mykyta.library.exceptions;

public class RepositoryLayerException extends RuntimeException{
    public RepositoryLayerException() {
        super();
    }

    public RepositoryLayerException(String message){
        super(message);
    }
}
