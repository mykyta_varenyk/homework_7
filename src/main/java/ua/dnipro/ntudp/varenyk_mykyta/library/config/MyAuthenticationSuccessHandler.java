package ua.dnipro.ntudp.varenyk_mykyta.library.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.MyUserPrincipal;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final UserDao userDao;

    @Autowired
    public MyAuthenticationSuccessHandler(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        Object userPrincipal = authentication.getPrincipal();

        if (userPrincipal instanceof MyUserPrincipal) {
            String login = ((MyUserPrincipal) userPrincipal).getUsername();

            User user = userDao.findUserByLogin(login);

            httpServletRequest.getSession(false).setAttribute("user", user);

            if (Role.valueOf(user.getRole().toUpperCase()) == Role.USER) {
                httpServletResponse.sendRedirect("/FinalProject/controller/user/home");
            }

            if (Role.valueOf(user.getRole().toUpperCase()) == Role.ADMIN){
                httpServletResponse.sendRedirect("/FinalProject/controller/admin/home");
            }

            if (Role.valueOf(user.getRole().toUpperCase()) == Role.LIBRARIAN){
                httpServletResponse.sendRedirect("/FinalProject/controller/librarian/home");
            }
        }
    }
}
