<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Librarian/Orders</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body>
<div class="page-wrapper">
    <div class="container">

        <div class="row">
            <c:forEach var="bean" items="${requestScope.userOrderBeans}">
            <c:if test="${bean.librarianId == 0}">
            <div class="order-item">
                <div class="row">
                    <div class="col s9">
                        <div class="row">
                            <div class="col s3">
                                ID:
                                <span>${bean.orderId}</span>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.name"/>
                                    <span>${bean.name}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.publisher"/>
                                <span>${bean.publisher}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.author"/>
                                <span>${bean.author}</span>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${bean.daysCount != 0}">
                                <div class="row">
                                    <div class="col s3">
                                        <fmt:message key="label.daysCount"/>
                                        <span>${bean.daysCount}</span>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col s3">
                                    <fmt:message key="label.hoursCount"/>
                                    <span>${bean.hoursCount}</span>
                                </div>
                            </c:otherwise>
                        </c:choose>
                        <div class="row">
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller/librarian/approve-order">
                                <input type="number" hidden name="id" value="${bean.orderId}"/>
                                <input type="submit" class="btn"
                                       value="<fmt:message key="button.approve"/>"/>
                            </form>
                        </div><br/>

                        <div class="row">
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller/librarian/disapprove-order">
                                <input type="number" hidden name="id" value="${bean.orderId}"/>
                                <input type="submit" class="btn"
                                       value="<fmt:message key="button.disapprove"/>"/>
                            </form>
                        </div>
                        </div><br/>

                    </div>
                </div>
            </div>
        </div>
    </c:if><br/>
        </c:forEach>
    </div>
<ul class="pagination">
    <c:if test="${requestScope.currentPage != 1}">
        <li class="disabled">
            <a href="${pageContext.request.contextPath}/controller/librarian/orders?page=${requestScope.currentPage-1}">  Previous
            </a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${requestScope.numOfPages}" varStatus="i">
        <c:choose>
            <c:when test="${requestScope.currentPage eq i.index}}">
                <li class="page-active">
                    <a class="page-link">
                            ${i.index}<span class="sr-only">(current)</span>
                    </a>
                </li>
            </c:when>
            <c:otherwise>
                <li class="page-active">
                    <a class="page-link" href="${pageContext.request.contextPath}/controller/librarian/orders?page=${i.index}">${i.index}</a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <c:if test="${requestScope.currentPage lt requestScope.numOfPages}">
        <li class="page-item">
            <a class="page-link" href="${pageContext.request.contextPath}/controller/librarian/orders?page=${requestScope.currentPage+1}">Next</a>
        </li>
    </c:if>
</ul>

</div><br/>
</div>
<h:change-language path="redirect:/FinalProject/controller/librarian/orders"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
