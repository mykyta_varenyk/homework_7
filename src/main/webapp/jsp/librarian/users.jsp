<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Users</title>
</head>
<body>
<div class="page-wrapper">
    <div class="container">

        <div class="row">
            <c:forEach var="user" items="${requestScope.users}">
                <c:if test="${user.roleId == 2}">
                <div class="user-item">
                    <div class="row">
                        <div class="col s9">
                            <div class="row">
                                <div class="col s3">
                                    ID:
                                    <span>${user.id}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.email"/>
                                    <span>${user.email}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.login"/>
                                    <span>${user.login}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.role"/>
                                    <span>${user.role}</span>
                                </div>
                            </div>
                            <c:choose>
                                <c:when test="${sessionScope.language == 'ua'}">
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.firstName"/>
                                    <span>${user.firstNameUa}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.lastName"/>
                                    <span>${user.lastNameUa}</span>
                                </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="row">
                                        <div class="col s3">
                                            <fmt:message key="label.firstName"/>
                                            <span>${user.firstName}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3">
                                            <fmt:message key="label.lastName"/>
                                            <span>${user.lastName}</span>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            <div class="row">
                                <div class="col s3">
                                    <c:choose>
                                        <c:when test="${user.hasOrders == false}">
                                           <fmt:message key="users_jsp.noOrders"/>
                                        </c:when>
                                        <c:otherwise>
                                            <form method="post"
                                                  action="${pageContext.request.contextPath}/controller/librarian/orders-of-user?id=${user.id}">
                                                <input type="submit" class="btn"
                                                       value="view orders"/>
                                            </form>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br/>
            </c:if>
            </c:forEach>
        </div>
    </div>
</div>
</div>
<h:change-language path="redirect:/FinalProject/controller/librarian/users"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
