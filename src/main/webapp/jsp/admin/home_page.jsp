<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Admin home page</title>
</head>
<body>
<fmt:message key="label.userGreeting"/>, ${sessionScope.user.login}!
<br/><br/><br/>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="user-item">
                <div class="row">
                    <div class="col s9">
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.email"/>
                                <span>${user.email}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                               <fmt:message key="label.login"/>
                                <span>${user.login}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.firstName"/>
                                <span>${user.firstName}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.lastName"/>
                                <span>${user.lastName}</span>
                            </div>
                        </div><br/>
                    </div>
                </div><br/>
      <form method="get" action="${pageContext.request.contextPath}/controller/admin/books">
          <input class="btn" type="submit" value="Edit books">
      </form>
        <br/>

        <form method="get" action="${pageContext.request.contextPath}/controller/admin/users">
            <input class="btn" type="submit" value="Edit users">
        </form>
            </div>
        </div>
    </div>

    <h:change-language path="redirect:/FinalProject/controller/admin/home"/>
    <%@include file="/WEB-INF/jspf/footer.jspf"%>
</div>
</body>
</html>