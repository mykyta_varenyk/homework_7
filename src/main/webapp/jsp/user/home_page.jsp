<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>


<html>
<head>
    <title>User home page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/index.css">
</head>
<body>
<div class="wrapper">
<%@include file="/WEB-INF/jspf/header.jspf"%>
<main>
    <br/><br/><br/>
    <div class="wrapper">
        <h1> <fmt:message key="label.userGreeting"/>, ${sessionScope.myUserPrincipal.username}!</h1>
        <div class="container">
            <div class="row">
                <div class="user-item">
                    <div class="row">
                        <div class="col s9">
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.email"/>
                                    <span>${sessionScope.user.email}</span>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.login"/>
                                    <span>${sessionScope.user.login}</span>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.firstName"/>
                                    <span>${sessionScope.user.firstName}</span>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.lastName"/>
                                    <span>${sessionScope.user.lastName}</span>
                                </div>
                            </div><br/>
                        </div>
                    </div><br/>
                </div>
            </div>
        </div>
        <h:change-language path="redirect:/FinalProject/controller/user/home"/>
        <%@include file="/WEB-INF/jspf/footer.jspf"%>
    </div>
    </main>
</div>
</body>
</html>